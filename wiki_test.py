import pytest

# from wiki import page_api_get

# from flask import url_for, request

import wiki


@pytest.fixture
def client():
    wiki.app.config["TESTING"] = True

    with wiki.app.test_client() as client:
        yield client


def test_import():
    assert wiki is not None


def test_homepage(client):
    resp = client.get("/")
    assert resp.status_code == 200


def test_front_page(client):
    resp = client.get("/view/", follow_redirects=True)
    assert resp.status_code == 200
    # assert request.path == url_for('handle_page', PageName="FrontPage")
    resp = client.get("/view/", follow_redirects=False)
    assert resp.status_code == 302


def test_page_name(client):
    resp = client.get("/view/test")
    # directory = "templates/test.html"
    # with open(directory, "rb") as f:
    # text = f.read()
    assert resp.status_code == 200
    # assert text[0:55] == resp.data[0:55]
    # after index 55 there is a issue with
    # \n spaced, but has no practical impact


def test_edit_page(client):
    resp = client.get("/edit/EditingTest")
    assert resp.status_code == 200


def test_edit_new_page(client):
    resp = client.post(
        "/edit/NewPage",
        data=dict(name="", email="", description="", edit=""),
        follow_redirects=True,
    )
    assert resp.status_code == 200


def test_edit_404_page(client):
    resp = client.get("/view/Nonexistent")
    assert resp.status_code == 404
    assert b"<a href=/edit/Nonexistent>here</a>" in resp.data


def test_history(client):
    resp = client.get("/history/EditingTest")

    with open("logs/EditingTest.csv", "r") as file:
        contents = file.read()

    assert resp.status_code == 200
    assert "Date&Time , Name , Email , Description" in contents


def test_no_history(client):
    resp = client.get("/history/HistoryTest")
    assert resp.status_code == 200
    assert b"No history has been found for this page." in resp.data
