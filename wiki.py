from flask import Flask, render_template, redirect, url_for, request
from format import parser, convert_hmtl
from datetime import datetime
from typing import Tuple
import csv
import os

app = Flask(__name__)


@app.route("/")
def main():
    return render_template("home.html")


@app.route("/main/<page_name>/<page_content>/")
def handle_request(
    page_name: str, page_content: str, edit_link: str, history_link: str
) -> Tuple[str, int]:
    # TODO: load the desired page content
    return (
        render_template(
            "main.html",
            page_name=page_name,
            contents=page_content,
            edit_link=edit_link,
            history_link=history_link,
        ),
        200,
    )


@app.route("/view/")
def handle_front_page():
    return redirect(url_for("handle_page", PageName="FrontPage"))


@app.route("/view/<PageName>")
def handle_page(PageName: str) -> Tuple[str, int]:
    # directory = "pages/" +PageName +".txt"
    # with open(directory, "r") as f:
    #     text = f.read()
    # return text
    try:
        arr = parser(PageName)
    except (FileNotFoundError):
        return (
            render_template(
                "edit404.html",
                edit_link="<a href=/edit/" + PageName + ">here</a>",
            ),
            404,
        )

    page_content = convert_hmtl(arr)
    edit_link = "<a href=/edit/" + PageName + ">Edit Page</a>"
    history_link = "<a href=/history/" + PageName + ">Page History</a>"
    return handle_request(
        page_name=PageName,
        page_content=page_content,
        edit_link=edit_link,
        history_link=history_link,
    )


@app.route("/edit/<page_name>", methods=["GET", "POST"])
def edit_page(page_name):
    """
    Allows appending text to wiki pages

    Display a text box with raw content of the wiki page to edit, appending
    user input to the page content by using html form tag and calling existing
    format methods. Also, collects name and email for future history tracking.

    Args:
        page_name: String name of the page to edit
        TODO: Allow editing of page contents and add history of changes made

    Returns:
        Edits reformatted to fit html syntax for rendering templates
    """
    if request.method == "POST":
        name = request.form.get("name")
        email = request.form.get("email")
        description = request.form.get("description")
        edit = request.form.get("edit")

        with open("pages/" + page_name + ".txt", "w") as file:
            file.write(edit)
            file.write("\n")
            convert_hmtl(parser(page_name))

        log_file = "logs/" + page_name + ".csv"
        with open(log_file, "a+") as log:
            if os.stat(log_file).st_size == 0:
                log.write(
                    "Date&Time , Name , Email , Description \n"
                )  # pragma: no cover
            log.write(
                str(datetime.now())
                + ","
                + name
                + ","
                + email
                + ","
                + description
                + "\n"
            )

        return redirect(url_for("handle_page", PageName=page_name))

    # If this line is reached, request is "GET"
    page_path = "pages/" + page_name + ".txt"

    if os.path.exists(page_path):
        with open(page_path, "r+") as file:
            content = file.read()
    else:
        content = "@h1 This is a new page being created (Please delete this line) ;;"  # pragma: no cover

    view_link = "<a href=/view/" + page_name + ">View Page</a>"
    history_link = "<a href=/history/" + page_name + ">Page History</a>"
    return (
        render_template(
            "edit.html", content=content, view_link=view_link, history_link=history_link
        ),
        200,
    )


@app.route("/history/<page_name>")
def history_page(page_name):
    csv_log_content = []
    try:
        with open("logs/" + page_name + ".csv", "r") as log:
            dict_reader = csv.DictReader(log)
            csv_log_content = list(dict_reader)

    except (FileNotFoundError):
        return "No history has been found for this page."

    view_link = "<a href=/view/" + page_name + ">View Page</a>"
    edit_link = "<a href=/edit/" + page_name + ">Edit Page</a>"

    return (
        render_template(
            "history.html",
            parent_list=csv_log_content,
            number=len(csv_log_content),
            view_link=view_link,
            edit_link=edit_link,
        ),
        200,
    )


@app.route("/api/v1/page/<page_name>/get")
def page_api_get(page_name):  # pragma: no cover
    format = request.args.get("format", "all")
    # TODO: implement response
    status_code = 200
    if format == "raw":
        if os.path.isfile("pages/" + page_name + ".txt"):
            with open("pages/" + page_name + ".txt", "r") as f:
                text = f.read()
            json_response = {"success": "true", "raw": text}
        else:
            status_code = 404
            json_response = {"success": "false", "reason": "Page does not exist"}
    elif format == "html":
        if os.path.isfile("templates/" + page_name + ".html"):
            with open("templates/" + page_name + ".html", "r") as f:
                text = f.read()
            json_response = {"succes": "true", "html": text}
        else:
            status_code = 404
            json_response = {"succes": "false", "reason": "Page does not exist"}
    elif format == "all":
        if os.path.isfile("pages/" + page_name + ".txt") and os.path.isfile(
            "templates/" + page_name + ".html"
        ):
            with open("pages/" + page_name + ".txt", "r") as f:
                raw = f.read()
            with open("templates/" + page_name + ".html", "r") as f2:
                html = f2.read()
            json_response = {"success": "true", "raw": raw, "html": html}
        else:
            status_code = 404
            json_response = {"success": "false", "reason": "Page does not exist"}
    else:
        status_code = 400
        json_response = {"success": "false", "reason": "Unsupported format"}
    return json_response, status_code
