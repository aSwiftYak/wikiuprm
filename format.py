# from flask import Flask
from typing import List


def parser(filename: str) -> List[List[str]]:
    text = ""
    filename = "pages/" + filename + ".txt"
    with open(filename, "r") as file:
        text = file.read().replace("\n", "")

    i = 0
    length = len(text)
    lst = []
    Another = False
    # Making list of lists
    while i < length:
        if text[i] == "@":
            i += 1
            temp_lst = []
            # Adding labels
            if i < length and i + 1 < length:
                if text[i] == "p":
                    temp_lst.append("p")
                    i += 1
                else:
                    label = text[i] + text[i + 1]
                    i += 2
                    if label == "h1":
                        temp_lst.append("h1")
                    elif label == "h2":
                        temp_lst.append("h2")
                    elif label == "li":
                        if i < length and i + 1 < length:
                            if label + text[i] + text[i + 1] == "link":
                                i += 2
                                temp_lst.append("link")
                            else:
                                raise Exception("Mispelled link")
                        else:
                            raise Exception("Mispelled link")
                    else:
                        # Removing label
                        raise Exception("Unknown Label")
            else:
                raise Exception

            # Adding string
            temp_str = ""
            end = False
            i += 1
            while not end and (i < length and i + 1 < length):
                # A link can be inside of text
                if text[i] == "@" and i + 4 < length:
                    if text[i + 1] + text[i + 2] + text[i + 3] + text[i + 4] == "link":
                        i += 5
                        temp_lst.append(temp_str)
                        temp_lst.append("link")
                        temp_str = ""
                        finished = False
                        while not finished and i + 3 < length:
                            if text[i] == ";" and text[i + 1] == ";":
                                i += 2
                                temp_lst.append(temp_str)
                                finished = True
                            else:
                                temp_str += text[i]
                                i += 1
                        temp_str = ""
                        Another = True

                if text[i] == ";" and text[i + 1] == ";":
                    i += 2
                    if temp_str is not None and temp_str != " ":
                        if Another:
                            temp_lst.append(temp_lst[0])
                            Another = False
                        temp_lst.append(temp_str)
                    end = True
                else:
                    temp_str += text[i]
                    i += 1
            lst.append(temp_lst)
        else:
            # Ignoring anything that doesn't start with @
            i += 1
    return lst


def convert_hmtl(arr: List[List[str]]) -> str:
    result = """"""
    for x in arr:

        if len(x) == 2:
            if x[0] == "link":
                label = '<a href="{0}">'.format(x[1])
                label.replace("' ", "'")

                content = x[1]
                endlabel = "</a>"
            else:
                label = "<" + x[0] + ">"
                content = x[1]
                endlabel = "</" + x[0] + ">"
            line = label + content + endlabel + "\n"
            result = result + line
        else:
            label = "<" + x[0] + ">"
            content = x[1]
            endlabel = "</" + x[0] + ">"
            line = label + content

            for i in range(2, len(x), 2):
                if x[i] == x[0]:
                    line = line + x[i + 1]
                else:
                    if x[i] == "link":
                        extra_label = '<a href="{0}">'.format(x[i + 1])
                        extra_content = x[i + 1]
                        extra_endlabel = "</a>"
                    else:
                        extra_label = "<" + x[i] + ">"
                        extra_content = x[i + 1]
                        extra_endlabel = "</" + x[i] + ">"
                    line = line + extra_label + extra_content + extra_endlabel
            line = line + endlabel + "\n"
            result = result + line

    return result
