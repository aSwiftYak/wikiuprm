import pytest
import format


def test_import():
    assert format is not None


def test_parser():
    lst = format.parser("ParserTest")

    # Testing for one whole statement
    assert lst[0][0] == "h1"

    # Testing for several statements
    assert lst[1][0] == "h2"
    assert lst[2][0] == "p"
    assert lst[3][0] == "link"

    # Testing link inside text
    assert lst[4][0] == "p"
    assert lst[4][2] == "link"
    assert lst[5][2] == "link"
    assert lst[5][4] == "p"
    assert lst[5][5] == "word at end"


def test_exception_parsert():
    with pytest.raises(Exception):
        format.parser("ParserTest2")


def test_parser_ifs():
    with pytest.raises(Exception):
        format.parser("ParserTest3")
    with pytest.raises(Exception):
        format.parser("ParserTest5")


def test_non_allowed_label():
    with pytest.raises(Exception):
        format.parser("ParserTest4")


@pytest.mark.parametrize(
    ["contents", "output"],
    [
        (
            [["h1", "This is header"], ["p", "This is paragraph"], ["link", "Link"]],
            """<h1>This is header</h1>
<p>This is paragraph</p>
<a href="Link">Link</a>
""",
        ),
        (
            [["h1", "Test"], ["p", "This has a ", "link", "link", "p", "inside of it"]],
            """<h1>Test</h1>
<p>This has a <a href="link">link</a>inside of it</p>
""",
        ),
        (
            [["p", "Goldfish are decendants of", "link", "carps"]],
            """<p>Goldfish are decendants of<a href="carps">carps</a></p>
""",
        ),
        (
            [["p", "This has a ", "h1", "header", "p", "inside of it"]],
            """<p>This has a <h1>header</h1>inside of it</p>
""",
        ),
    ],
)
def test_convert_html(contents, output):
    result = format.convert_hmtl(contents)
    print(output)
    print(result)
    assert result == output
